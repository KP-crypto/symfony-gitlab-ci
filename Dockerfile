FROM php:7.4-apache

RUN apt-get update && apt-get install -y \
  default-mysql-client \
  ffmpeg \
  gettext \
  git \
  imagemagick \
  mediainfo \
  supervisor \
  vim \
  wget \
  zip \
  && rm -rf /var/lib/apt/lists/*

RUN curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
    chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions apcu exif gd imagick pcntl pdo_mysql sockets zip

ARG APP_ENV
RUN if ! [[ "$APP_ENV" = "prod" ]] ; then install-php-extensions xdebug ;  fi

COPY docker/httpd.conf /etc/apache2/sites-available/onemedia.conf
RUN a2ensite onemedia \
    && a2dissite 000-default \
    && a2enmod rewrite

COPY docker/php.ini /usr/local/etc/php/conf.d/onemedia.ini
COPY docker/supervisord.conf /etc/supervisor/conf.d/onemedia.conf

WORKDIR /var/www/html
COPY . /var/www/html
RUN chmod 777 /var/www/html/var/cache /var/www/html/var/log

COPY --from=composer:2.2.18 /usr/bin/composer /usr/local/bin/composer
RUN cd /var/www/html \
    && composer install --no-interaction --no-scripts

RUN cd /root/ \
    && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install -b /bin \
    && rm -rf awscliv2.zip aws \
    && chmod 777 /root

EXPOSE 80
